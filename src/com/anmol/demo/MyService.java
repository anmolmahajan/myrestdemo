package com.anmol.demo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@Path("/my")
public class MyService {
	
	static Map<Integer, Employee> m = null;
	
	static {
		
		m = new HashMap<>();
		Employee e1 = new Employee();
		e1.setId(1);
		e1.setName("Harsh");
		e1.setSalary(1000);
		
		Employee e2 = new Employee();
		e2.setId(2);
		e2.setName("Sung");
		e2.setSalary(1001);
		
		m.put(e1.getId(), e1);
		m.put(e2.getId(), e2);
		
	}
	
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/first")
	public String firstService() {
		return "Hello World";
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/second/{na}")
	public String secondService(@PathParam("na")String name) {
		return "Hello " + name;
	}
	
	
	@GET
	@Produces(MediaType.TEXT_XML)
	@Path("/employees")
	public ArrayList<Employee> getAllEmployees(){
		return new ArrayList<Employee>(m.values());
	}
	
}
